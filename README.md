<h1>kameplayer ver4</h1>

プレイヤーに色々させるサーバー管理用コマンド追加プラグイン(管理プラグイン)<br>

* [ダウンロード kameplayer](https://gitlab.com/kameme1208/kameplayer/issues)

<h1>機能一覧</h1>
* ***CommandBlockの実行時の挙動をオリジナルのものに変更する機能***<br>
  **プラグインで追加されたコマンド**をコマンドブロックから実行したとき<br>
  コマンドが実行されたかされてないか(コマンドの戻り値を無視)というデフォルトの仕様から<br>
  コマンドで戻ってきた値(真or偽)で判断しその成功回数をSuccessCountに設定する方法に変更<br>
  
 * **この機能の用途**<br>
   プラグインで追加されたコマンドがコマンドブロックの`条件付き`の設定をしても常時実行されてしまうのをバニラコマンドと同じように動かせるように<br>
   (**他人のプラグイン**ではどういう戻り方をしてるのかわからないので、**必ず動くとは限りません**、私のプラグインではほぼ条件がつけられます)(**コンパレータ**を付けた場合も)<br>
   サーバー本体の**クラスを書き換え**、コマンドブロックの**挙動が変更**されるのでデフォルトでは**無効**になっています<br>


* ***地下埋め機能(重いかも)***<br>
  有効にすることで**指定したワールドでチャンクが新規に生成されたとき**Y=64以下かつ明るさが0のオプションで指定したIDのブロックをを**すべて石に置き換える**<br>
  地下の鉱石や洞窟を埋めるのに便利(廃鉱や地下の水たまりが一部消えない場合もある)

* ***チャンクローダー***<br>
  チャンクが凍結しないように起動時から読み込み続けるブロックを追加します /kameplayer chunkloaderでアイテムが呼び出せます


<h1>開発者向け</h1>
　いくつか便利な呼び出しができる機能を追加しています。<br>
　
* **NBTタグの値の取得・設定**<br>

```java
    //NBTの取得
    Object obj = NBTUtils.getNBTTag(ItemStack|Entity|Block ,NBTTagName);
    
    //Objectから値へ変換(型に変換できなければ空の物or2個目の引数が戻る)
    Map<String, Object> nbtmap = NBTUtils.getMap(obj);
    List<Object> nbtlist = NBTUtils.getList(obj);
    Number nbtnumber = NBTUtils.getNumber(obj, default);
    String nbtstring = NBTUtils.getString(obj, default);
    
    //NBTの書き込み
    NBTUtils.setNBTTag(ItemStack|Entity|Block , NBTTagName, Map|List|Number|String);
    
    //ItemStack <=> Map変換
    Map<String, Object> itemobject = NBTUtils.toMap(ItemStack);
    ItemStack item = NBTUtils.createItemStack(Map<String, Object>);
```

* **NBTEditor(複数のNBTを一気に操作する場合早い(っぽい))**<br>

```java
    //エディタ?の起動
    NBTEditor<T> editor = NBTEditor.open(ItemStack|Block|Entity);
    
    //キーの取得
    Set<String> keys = editor.getKeys();
    //指定文字のキーが含まれているか
    boolean bool = editor.containsKey(String);
    //NBTタグのバッファへの書き込み(Map,Collection,String,Number)
    Void voivoi = editor.set(KeyString, Object);
    //NBTタグ値の取得
    Object obj = editor.get(KeyString);
    //NBTをすべてMapへ変換
    Map<String, Object> map = editor.toMap();
    //NBTの書き込み(flush -> 元のオブジェクトへ書き込み)
    T output = editor.flush();
    //NBTの書き込み(copy -> 元のオブジェクトはそのままに複製して書き込み(ItemStackのみ))
    T output = editor.copy();
```
* **2点間の直線と地形・エンティティとの衝突判定**<br>

```java
    //地点A から 地点B までの中で指定した仮想の箱(2点を対角とする四角柱)との衝突点の取得
    CollisionPosition pos = Collider.rayTrace(startLocation, endLocation, boxLocation1, boxLocation2);
    //地点A から 地点B までの地形との衝突点とその間にいるエンティティとの衝突点の取得
    List<CollisionPosition> pos = Collider.rayTraceWorld(startLocation, endLocation, skipList(Entity), boolean, boolean, boolean, Class<? extends Entity>);
    
    //CollisionPositionから取得できる値
    boolean bool = pos.isHit(); //衝突したかどうか
    boolean bool = pos.isBlock(); //ブロックかどうか
    boolean bool = pos.isEntity(); //エンティティかどうか
    boolean bool = pos.isBox(); //仮想の箱かどうか
    Block block = pos.getBlock(); //ブロックの取得()※1
    Entity entity = pos.getEntity(); //エンティティの取得()※1
    Location loc = pos.getPosition(); //衝突した場所の取得()※1
    BlockFace face = pos.getDirection(); //衝突した面の取得()※1
```
※1 : 衝突していない場合はnull

* **Actionbarのメッセージ送信**<br>

```java
	KameAPI.sendActionbar(player, message);
```

* **KameAPIオリジナルカスタム耐久の操作**<br>

```java
    Result result = KameAPI.itemDamage(Player, ItemStack, damage);
    //カスタム耐久の値を持っていないアイテム = Result.DEFAULT
    //カスタム耐久が適応され耐久が0出ない場合 = Result.ALLOW
    //カスタム耐久が適応され耐久が0になった場合 = Result.DENY
```

<h1>追加コマンド</h1>
| コマンド|使用例| 説明 |
|-----|-----|-----|
|kameplayer|/kameplayer chunkloader|小物コマンド|
|commandblock|/commandblock add TEST|コマンドでコマンドブロックに文字を追加削除します|
|attack|/attack player 10 10|指定したエンティティに指定プライヤーからの攻撃としてダメージを与えます|
|entityrun|/entityrun ~ ~ ~ Bat /setblock ~ ~ ~ minecraft:redstoneblock|死亡時にコマンドが実行されるエンティティを召喚します{ある程度NBT可能}|
|copy||クローンのようなものです(元1.7用)|
|vec|/vec @p ~ ~1 ~|プレイヤーにコマンドから加速度を与えます|
|armor|/armor player (repair)|防具を着たまま修理、手持ちを着せたりできます|
|drop|/drop player SAND|プレイヤーの指定アイテムを地面に落とします|
|entity|/entity remove player 10|エンティティにダメージを与えたり消したりできます|
|firework|/firework 1~3|ランダムの設定の花火を打ち上げます|
|explode|/explode loc ~ ~ ~ 1|爆発を起こします|
|itemrun|/itemrun player item amount data {NBT} /command ｜ /command...|そのアイテムを持ってた際にコマンドを実行します、アイテムを無効にし、コマンド連結だけで1つのコマンドにまとめることも可能です|
|itemfix|/itemfix player setlore inhand - text|プレイヤーのアイテムのLore、名前等を変更します
|respawn|/respawn set world x y z keep?|自動リスポーン、一時リスポーンを設定します、ベッドスポーンは変更されません|
|broadcast|/broadcast all text|tellrawの拡張？(1.7では)
|guidelines|/guidelines|ガイドラインを表示させるコマンド|
|tpto|/tpto @p x y z yaw pitch world|角度指定のテレポート|
|vacuum|/vacuum on 5|アイテム吸い寄せ|
|tellbar|/tellbar @p text|アクションバーを表示させるコマンド|
|invclose|/invclose player|インベントリを閉じさせるコマンド|
|trig|/trig @p amount flag ｜& \^ ?...|スコアボードトリガー、trigに指定数をセットする、プレイヤーの状態も判断可能|
|pull|/pull player item {nbt}|give互換、ただしこっちはスタック量無視や、音が鳴らないようになってる|
|pulse|/pulse 20 10 1 -|コマンドブロックのみで指定周期のパルスを作れるクロックの軽量化目的(CustomCommandMapを推奨)|
|timescore|/timescore start.stop.put.reset|時間を測定(0.1秒精度)|
|dice|/dice xDxx SCORE?|ダイスコマンド|
|compute|/compute player item amount data {NBT} keep/consume {o:["cmd","成功時"],x:["cmd","失敗時"]}|itemrun後継コマンド|
|compute|/computepattern player item amount data {NBT} keep/consume {o:["cmd","成功時"],x:["cmd","失敗時"]}|computeコマンドのNBTに正規表現を使用するモード|