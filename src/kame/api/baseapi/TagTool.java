package kame.api.baseapi;

import static java.lang.annotation.ElementType.*;

import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

public class TagTool {
	/**
	 * The value received on this object is duplicated.<br>
	 * Change this value will not be reflected in the original object.<br>
	 */
	@Target(METHOD)
	private @interface Copied {

	}

	@Target(METHOD)
	private @interface Mirrored {

	}

	@Target(METHOD)
	private @interface Otherobj {

	}

	public static class TagConverter {

		@Nonnull@Copied
		public static Map<String, Object> toMap(Object tagObject) {
			if(tagObject instanceof Map) {
				Map<String, Object> map = new HashMap<>(((Map<?, ?>) tagObject).size());
				((Map<?, ?>) tagObject).forEach((x, y) -> map.put(x.toString(), y));
				return map;
			}else {
				return new HashMap<>();
			}
		}

		@Nonnull@Copied
		public static <T>Map<String, T> toMap(Object tagObject, Class<T> valueClass) {
			if(tagObject instanceof Map) {
				Map<String, T> map = new HashMap<>(((Map<?, ?>) tagObject).size());
				((Map<?, ?>)tagObject).forEach((x, y) -> {if(valueClass.isInstance(y))map.put(x.toString(), valueClass.cast(y));});
				return map;
			}else {
				return new HashMap<>();
			}
		}

		@Nonnull@Copied
		public static List<Object> toList(Object tagObject) {
			if(tagObject instanceof List) {
				return new ArrayList<Object>((List<?>) tagObject);
			}else {
				return new ArrayList<>();
			}
		}

		@Nonnull@Copied
		public static <T>List<T> toList(Object tagObject, Class<T> valueClass) {
			if(tagObject instanceof List) {
				List<T> list = new ArrayList<>(((List<?>) tagObject).size());
				((List<?>)tagObject).forEach(x -> {if(valueClass.isInstance(x))list.add(valueClass.cast(x));});
				return list;
			}else {
				return new ArrayList<>();
			}
		}

		public static <T>T get(Object tagObject, Class<T> classFilter, T def) {
			return tagObject != null && classFilter.isInstance(tagObject) ? classFilter.cast(tagObject) : def;
		}
	}

	private static final String VERSION = Bukkit.getServer().getClass().getPackage().getName().replaceFirst(".*(\\d+_\\d+_R\\d+).*", "$1");

	//CraftItemStack-----------------------------------------------
	private static final Class<?> Class_CraftItemStack;
	private static final Method Method_asNMSCopy;
	private static final Method asCraftMirror;

	//NMSItemStack-------------------------------------------------
	private static final Class<?> Class_NMSItemStack;
	private static final Field Field_NMSItemStack_NBTTagCompound;
	private static final Field Field_NBTTagListType;

	//NBTTagCompound_Class-----------------------------------------
	private static final Class<?> Class_NBTBase;

	private static final Class<?> Class_NBTTagByte;
	private static final Class<?> Class_NBTTagShort;
	private static final Class<?> Class_NBTTagInt;
	private static final Class<?> Class_NBTTagLong;
	private static final Class<?> Class_NBTTagFloat;
	private static final Class<?> Class_NBTTagDouble;
	private static final Class<?> Class_NBTTagByteArray;
	private static final Class<?> Class_NBTTagString;
	private static final Class<?> Class_NBTTagList;
	private static final Class<?> Class_NBTTagCompound;
	private static final Class<?> Class_NBTTagIntArray;

	//NBTTagCompound_Field-----------------------------------------
	private static final Field Field_NBTTagByte;
	private static final Field Field_NBTTagShort;
	private static final Field Field_NBTTagInt;
	private static final Field Field_NBTTagLong;
	private static final Field Field_NBTTagFloat;
	private static final Field Field_NBTTagDouble;
	private static final Field Field_NBTTagByteArray;
	private static final Field Field_NBTTagString;
	private static final Field Field_NBTTagList;
	private static final Field Field_NBTTagCompound;
	private static final Field Field_NBTTagIntArray;

	//NBTTagCompound_Constructor-----------------------------------------

	private static final Constructor<?> Instance_NBTTagByte;
	private static final Constructor<?> Instance_NBTTagShort;
	private static final Constructor<?> Instance_NBTTagInt;
	private static final Constructor<?> Instance_NBTTagLong;
	private static final Constructor<?> Instance_NBTTagFloat;
	private static final Constructor<?> Instance_NBTTagDouble;
	private static final Constructor<?> Instance_NBTTagByteArray;
	private static final Constructor<?> Instance_NBTTagString;
	private static final Constructor<?> Instance_NBTTagIntArray;

	//NBTTagTypeId

	private static final Map<Class<?>, Class<?>> classMap = new HashMap<>();
	private static final Map<Class<?>, Byte> classId = new HashMap<>();

	static {
		Class_NBTBase = fromNMSName("NBTBase");

		Class_NBTTagByte = fromNMSName("NBTTagByte");
		Class_NBTTagShort = fromNMSName("NBTTagShort");
		Class_NBTTagInt = fromNMSName("NBTTagInt");
		Class_NBTTagLong = fromNMSName("NBTTagLong");
		Class_NBTTagFloat = fromNMSName("NBTTagFloat");
		Class_NBTTagDouble = fromNMSName("NBTTagDouble");
		Class_NBTTagByteArray = fromNMSName("NBTTagByteArray");
		Class_NBTTagString = fromNMSName("NBTTagString");
		Class_NBTTagList = fromNMSName("NBTTagList");
		Class_NBTTagCompound = fromNMSName("NBTTagCompound");
		Class_NBTTagIntArray = fromNMSName("NBTTagIntArray");

		Instance_NBTTagByte = get_Constructor(Class_NBTTagByte, byte.class);
		Instance_NBTTagShort = get_Constructor(Class_NBTTagShort, short.class);
		Instance_NBTTagInt = get_Constructor(Class_NBTTagInt, int.class);
		Instance_NBTTagLong = get_Constructor(Class_NBTTagLong, long.class);
		Instance_NBTTagFloat = get_Constructor(Class_NBTTagFloat, float.class);
		Instance_NBTTagDouble = get_Constructor(Class_NBTTagDouble, double.class);
		Instance_NBTTagByteArray = get_Constructor(Class_NBTTagByteArray, byte[].class);
		Instance_NBTTagString = get_Constructor(Class_NBTTagString, String.class);
		Instance_NBTTagIntArray = get_Constructor(Class_NBTTagIntArray, int[].class);

		Field_NBTTagByte = fromClass(Class_NBTTagByte, byte.class);
		Field_NBTTagShort = fromClass(Class_NBTTagShort, short.class);
		Field_NBTTagInt = fromClass(Class_NBTTagInt, int.class);
		Field_NBTTagLong = fromClass(Class_NBTTagLong, long.class);
		Field_NBTTagFloat = fromClass(Class_NBTTagFloat, float.class);
		Field_NBTTagDouble = fromClass(Class_NBTTagDouble, double.class);
		Field_NBTTagByteArray = fromClass(Class_NBTTagByteArray, byte[].class);
		Field_NBTTagString = fromClass(Class_NBTTagString, String.class);
		Field_NBTTagList = fromClass(Class_NBTTagList, List.class);
		Field_NBTTagCompound = fromClass(Class_NBTTagCompound, Map.class);
		Field_NBTTagIntArray = fromClass(Class_NBTTagIntArray, int[].class);

		Class_CraftItemStack = fromName("org.bukkit.craftbukkit", "inventory.CraftItemStack");
		Method_asNMSCopy = fromName(Class_CraftItemStack, "asNMSCopy", ItemStack.class);

		Class_NMSItemStack = fromName("net.minecraft.server", "ItemStack");
		Field_NMSItemStack_NBTTagCompound = fromClass(fromName("net.minecraft.server", "ItemStack"), Class_NBTTagCompound);

		asCraftMirror = fromName(Class_CraftItemStack, "asCraftMirror", Class_NMSItemStack);
		Field_NBTTagListType = fromClass(Class_NBTTagList, byte.class);

	}

	private static Field fromClass(Class<?> target, Class<?> clazz) {
		putClassType(target, clazz);
		for(Field field : target.getDeclaredFields()) {
			if(field.getType().equals(clazz)) {
				field.setAccessible(true);
				return field;
			}
		}
		throw new RuntimeException(new NoSuchFieldException());
	}

	private static void putClassType(Class<?> target, Class<?> clazz) {
		try {
			Constructor<?> c = target.getDeclaredConstructor();
			c.setAccessible(true);
			Object obj = c.newInstance();
			if(Class_NBTBase.isInstance(obj)){
				byte type = (byte)target.getMethod("getTypeId").invoke(obj);
				classId.put(target, type);
				classId.put(clazz, type);
				classMap.put(target, clazz);
				classMap.put(clazz, target);
			}
		}catch(Exception e) {
			
		}
		
	}

	private static Constructor<?> get_Constructor(Class<?> target, Class<?>... classes) {
		try {
			return target.getConstructor(classes);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}

	}

	private static Class<?> fromNMSName(String name) {
		try {
			return Class.forName("net.minecraft.server.v" + VERSION + "." + name);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Class<?> fromName(String first, String last) {
		try {
			return Class.forName(first + ".v" + VERSION + "." + last);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Method fromName(Class<?> target, String name, Class<?>... classes) {
		try {
			return target.getMethod(name, classes);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	private static Object get_Field(Field field, Object obj) {
		if(obj == null)return null;
		try {
			return field.get(obj);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private static Object invoke_nmsCopy(ItemStack item) {
		try {
			return Method_asNMSCopy.invoke(item, item);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private static ItemStack invoke_craftMirror(Object nmsitem) {
		try {
			return (ItemStack) asCraftMirror.invoke(nmsitem, nmsitem);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * Instance
	 * 何回も同じアイテムにNBTの操作をするとき早いよ
	 */

	private final Object nmsitem;
	private final ItemStack bukkititem;

	public static TagTool open(ItemStack item) {
		return new TagTool(item);
	}

	public TagTool(ItemStack item) {
		bukkititem = item;
		nmsitem = invoke_nmsCopy(item);
		if(nmsitem == null)throw new NullPointerException("The material is not compatible \"net.minecraft.server.v" + VERSION + ".ItemStack\" / " + item);
	}

	public boolean hasTag(String tagName) {
		return hasKey(nmsitem, tagName);
	}

	public boolean hasTag(String tagName, Class<?> classFilter) {
		return hasKey(nmsitem, tagName, classFilter);
	}

	public Class<?> getType(String tagName) {
		return getTagType(nmsitem, tagName);
	}

	//読み込み-------------------------------------------

	@Copied
	public Set<String> keySet() {
		return getNBTKeys(nmsitem);
	}

	@Copied
	public Object get(String tagName) {
		return getNBTTag(nmsitem, tagName);
	}

	@Copied
	public <T>T get(String tagName, Class<T> classFilter, T defaultValue) {
		return getNBTTag(nmsitem, tagName, classFilter, defaultValue);
	}

	@Copied
	public Map<String, Object> getAllMap() {
		return getAllMap(nmsitem);
	}

	//書き込み-------------------------------------------

	@Copied
	public void set(String tagName, Object nbtTag) {
		setNBTTag(nmsitem, tagName, nbtTag);
	}

	@Copied
	public void remove(String tagName) {
		removeNBTTag(nmsitem, tagName);
	}

	@Copied
	public void clear(String tagName) {
		setNBTCompound(nmsitem, null);
	}

	@Mirrored
	public ItemStack flush() {
		bukkititem.setItemMeta(invoke_craftMirror(nmsitem).getItemMeta());
		return bukkititem;
	}

	@Copied
	public ItemStack copy() {
		return invoke_craftMirror(nmsitem);
	}
	@Mirrored
	public void setAllMap(Map<String, ?> nbtMap) {
		setAllMap(nmsitem, nbtMap);
	}

	/*
	 * static Method
	 * 一回だけの時とかに使う、手順を少なくできる
	 */

	@Otherobj
	public static boolean hasTag(@Nullable ItemStack item, String tagName) {
		return hasKey(invoke_nmsCopy(item), tagName);
	}

	@Otherobj
	public static boolean hasTag(@Nullable ItemStack item, String tagName, Class<?> classFilter) {
		return hasKey(invoke_nmsCopy(item), tagName, classFilter);
	}

	@Copied
	public static Set<String> keySet(@Nullable ItemStack item) {
		return getNBTKeys(invoke_nmsCopy(item));
	}

	@Otherobj
	public static Class<?> getType(@Nullable ItemStack item, String tagName) {
		return getTagType(invoke_nmsCopy(item), tagName);
	}

	@Copied
	public static Object get(@Nullable ItemStack item, String tagName) {
		return getNBTTag(invoke_nmsCopy(item), tagName);
	}

	@Copied
	public static <T>T get(@Nullable ItemStack item, String tagName, Class<T> classFilter, T defaultValue) {
		return getNBTTag(invoke_nmsCopy(item), tagName, classFilter, defaultValue);
	}

	@Copied
	public static <T>List<T> list(@Nullable ItemStack item, String tagName, Class<T> classFilter, List<T> putList) {
		return putNBTTag(invoke_nmsCopy(item), tagName, classFilter, putList);
	}

	@Copied
	public static <T>Map<String, T> map(@Nullable ItemStack item, String tagName, Class<T> classFilter, Map<String, T> putMap) {
		return putNBTTag(invoke_nmsCopy(item), tagName, classFilter, putMap);
	}

	@Mirrored
	public static void set(@Nullable ItemStack item, String tagName, Object nbtTag) {
		if(item != null)item.setItemMeta(invoke_craftMirror(setNBTTag(invoke_nmsCopy(item), tagName, nbtTag)).getItemMeta());
	}

	@Mirrored
	public static void remove(@Nullable ItemStack item, String tagName) {
		if(item != null)item.setItemMeta(invoke_craftMirror(removeNBTTag(invoke_nmsCopy(item), tagName)).getItemMeta());
	}

	@Mirrored
	public static void clear(@Nullable ItemStack item) {
		if(item != null)item.setItemMeta(null);
	}

	@Copied
	public static Map<String, Object> getAllMap(@Nullable ItemStack item) {
		return getAllMap(invoke_nmsCopy(item));
	}

	@Mirrored
	public static void setAllMap(@Nullable ItemStack item, Map<String, ?> nbtMap) {
		Object nmsitem = invoke_nmsCopy(item);
		if(nmsitem != null)setAllMap(nmsitem, nbtMap);
	}

	/*
	 * NMSItemStack -> NBTTagTool
	 * NMSのアイテムからNBTを色々前(後)処理するところ。
	 */

	@Copied @Nonnull
	private static Set<String> getNBTKeys(Object nmsitem) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)return new HashSet<>(0);
		Set<String> key = new HashSet<>(map.size());
		map.forEach((x, y) -> key.add(x.toString()));
		return key;
	}

	@Otherobj
	private static boolean hasKey(Object nmsitem, String key) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null ? map.containsKey(key) : false;
	}

	@Otherobj
	private static boolean hasKey(Object nmsitem, String key, Class<?> classFilter) {
		return classFilter.equals(getTagType(nmsitem, key));
	}

	@Otherobj @Nullable
	private static Class<?> getTagType(Object nmsitem, String key) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null && map.containsKey(key) ? classMap.get(map.get(key).getClass()) : null;
	}

	@Copied @Nullable
	private static Object getNBTTag(Object nmsitem, String tagName) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null ? getNBTTag(map.get(tagName)) : null;
	}

	@Copied @Nullable
	private static <T>T getNBTTag(Object nmsitem, String tagName, Class<T> classFilter, T defaultValue) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)return defaultValue;
		Object tagObject = map.get(tagName);
		if(tagObject == null)return defaultValue;
		Class<?> clazz = classMap.get(tagObject.getClass());
		if(clazz == null || !clazz.equals(classFilter))return defaultValue;
		return classFilter.cast(getNBTTag(tagObject));
	}

	@Copied @Nullable
	private static <T>List<T> putNBTTag(Object nmsitem, String tagName, Class<T> classFilter, List<T> putList) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)return putList;
		Object tagObject = getNBTTag(map.get(tagName));
		if(tagObject instanceof Collection)for(Object tag : (Collection<?>) tagObject) {
			if(classFilter.isInstance(tag))putList.add(classFilter.cast(tag));
		}
		return putList;
	}

	@Copied @Nullable
	private static <T>Map<String, T> putNBTTag(Object nmsitem, String tagName, Class<T> classFilter, Map<String, T> putMap) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)return putMap;
		Object tagObject = getNBTTag(map.get(tagName));
		if(tagObject instanceof Map)for(Entry<?, ?> entry : ((Map<?, ?>) tagObject).entrySet()) {
			if(classFilter.isInstance(entry.getValue()))putMap.put(entry.getKey().toString(), classFilter.cast(entry.getValue()));
		}
		return putMap;
	}

	@Copied @Nonnull
	private static Map<Object, Object> getNBTCopy(Object nmsitem) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map == null ? new HashMap<>() : new HashMap<>(map);
	}

	@Mirrored @Nullable
	private static Map<?, ?> getNBTMirror(Object nmsitem) {
		Object NBTTagCompound = get_Field(Field_NMSItemStack_NBTTagCompound, nmsitem);
		return (Map<?, ?>) get_Field(Field_NBTTagCompound, NBTTagCompound);
	}

	@Mirrored @Nullable @SuppressWarnings("unchecked")
	private static Map<String, Object> getEditableNBTMirror(Object nmsitem) {
		Object NBTTagCompound = get_Field(Field_NMSItemStack_NBTTagCompound, nmsitem);
		return (Map<String, Object>) get_Field(Field_NBTTagCompound, NBTTagCompound);
	}

	@Mirrored @Nullable
	private static Object setNBTTag(Object nmsitem, String tagName, Object tagObject) {
		Map<Object, Object> map = getNBTCopy(nmsitem);
		map.put(tagName, tagObject);
		setNBTCompound(nmsitem, map);
		return nmsitem;
	}

	@Mirrored
	private static Object removeNBTTag(Object nmsitem, String tagName) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map != null)map.remove(tagName);
		return nmsitem;
	}

	@Copied @Nonnull
	private static Map<String, Object> getAllMap(Object nmsitem) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)return new HashMap<>(0);
		Map<String, Object> newmap = new HashMap<>(map.size());
		map.forEach((x, y) -> newmap.put(x.toString(), getNBTTag(y)));
		return newmap;
	}

	@Mirrored
	private static void setAllMap(Object nmsitem, Map<String, ?> nbtMap) {
		setNBTCompound(nmsitem, nbtMap);
	}

	/*
	 * NMSItemStack -> NBTCompound
	 * NMSのアイテムから最浅部のみで変換したNBTタグを取得するところ。
	 */

	@Otherobj
	private static byte getTagTypeId(Object tagObject) {
		Byte id = tagObject == null ? null : classId.get(tagObject.getClass());
		return id == null ? -1 : id;
	}

	/*
	 * NMSItemStack -> NBTCompound
	 * NMSのアイテムからMap型のNBTをCompoundに変換しNBTタグに書き込むところ。
	 */

	@Mirrored
	private static void setNBTCompound(Object nmsitem, Map<?, ?> map) {
		try {
			Field_NMSItemStack_NBTTagCompound.set(nmsitem, setNBTTag(map));
		} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NullPointerException e) {
			throw new IllegalArgumentException("The material is not compatible \"net.minecraft.server.v" + VERSION + ".ItemStack\"");
		}
	}

	/*
	 * NBTBase -> Object
	 * 変換するところ。
	 */

	@Copied @Nullable
	private static Object getNBTTag(Object NBTTagCompound) {
		try {
			Byte type = classId.get(NBTTagCompound.getClass());
			if(type == null)return null;
			switch(type) {
			case 1:
				return Field_NBTTagByte.get(NBTTagCompound);
			case 2:
				return Field_NBTTagShort.get(NBTTagCompound);
			case 3:
				return Field_NBTTagInt.get(NBTTagCompound);
			case 4:
				return Field_NBTTagLong.get(NBTTagCompound);
			case 5:
				return Field_NBTTagFloat.get(NBTTagCompound);
			case 6:
				return Field_NBTTagDouble.get(NBTTagCompound);
			case 7:
				return Field_NBTTagByteArray.get(NBTTagCompound);
			case 11:
				return Field_NBTTagIntArray.get(NBTTagCompound);
			case 8:
				return Field_NBTTagString.get(NBTTagCompound);
			case 9:
				List<?> lists = ((List<?>)Field_NBTTagList.get(NBTTagCompound));
				List<Object> list = new ArrayList<>(lists.size());
				lists.forEach(x -> list.add(getNBTTag(x)));
				return list;
			case 10:
				Map<?, ?> maps = ((Map<?, ?>) Field_NBTTagCompound.get(NBTTagCompound));
				Map<String, Object> map = new HashMap<>(maps.size());
				maps.forEach((x, y) -> map.put(x.toString(), getNBTTag(y)));
				return map;

			default:
				throw new RuntimeException();
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * Object -> NBTBase
	 * 変換するところ。
	 */

	@Otherobj @Nullable
	private static Object setNBTTag(Object tagObject) throws IllegalAccessException, InstantiationException, InvocationTargetException {
		if(tagObject == null || Class_NBTBase.isInstance(tagObject)) {
			return tagObject;
		}else if(tagObject instanceof Boolean) {
			return Instance_NBTTagByte.newInstance(Byte.valueOf(tagObject.equals(true) ? (byte)1 : 0));
		}else if(tagObject instanceof Byte) {
			return Instance_NBTTagByte.newInstance(tagObject);
		}else if(tagObject instanceof Short) {
			return Instance_NBTTagShort.newInstance(tagObject);
		}else if(tagObject instanceof Integer) {
			return Instance_NBTTagInt.newInstance(tagObject);
		}else if(tagObject instanceof Long) {
			return Instance_NBTTagLong.newInstance(tagObject);
		}else if(tagObject instanceof Float) {
			return Instance_NBTTagFloat.newInstance(tagObject);
		}else if(tagObject instanceof Double) {
			return Instance_NBTTagDouble.newInstance(tagObject);
		}else if(tagObject instanceof byte[] || tagObject instanceof Byte[]) {
			return Instance_NBTTagByteArray.newInstance(tagObject);
		}else if(tagObject instanceof int[] || tagObject instanceof Integer[]) {
			return Instance_NBTTagIntArray.newInstance(tagObject);
		}else if(tagObject instanceof String) {
			return Instance_NBTTagString.newInstance(tagObject);
		}else if(tagObject instanceof List) {
			Object nbttag = Class_NBTTagList.newInstance();
			List<Object> list = new ArrayList<>(((List<?>) tagObject).size());
			Class<?> firstclass = null;
			for(Object array : (List<?>) tagObject) {
				if(firstclass == null) {
					firstclass = array.getClass();
				}else if(!firstclass.isInstance(array)){
					Bukkit.getLogger().log(Level.SEVERE, "配列内のクラスは1種類である必要があります。 "
							, new ClassCastException(array.getClass() + "can not cast " + firstclass));
				}
				if(array != null) {
					list.add(setNBTTag(array));
				}
			}
			Field_NBTTagList.set(nbttag, list);
			Field_NBTTagListType.set(nbttag, getTagTypeId(firstclass));
			return nbttag;
		}else if(tagObject instanceof Map) {
			Object nbttag = Class_NBTTagCompound.newInstance();
			Map<String, Object> map = new HashMap<>(((Map<?, ?>) tagObject).size());
			for(Entry<?, ?> entry : ((Map<?, ?>) tagObject).entrySet()) {
				Object value = setNBTTag(entry.getValue());
				if(value != null)map.put(entry.getKey().toString(), value);
			}
			Field_NBTTagCompound.set(nbttag, map);
			return nbttag;
		}
		return null;
	}

}
