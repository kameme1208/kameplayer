package kame.api.baseapi;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

public class NBTEditor<T> {

	private T neko;
	private Object NBTTag;
	private Object NMSObject;

	private NBTEditor(T neko) {
		this.neko = (T) neko;
	}

	public static NBTEditor<ItemStack> open(ItemStack item) {
		NBTEditor<ItemStack> editor = new NBTEditor<ItemStack>(item);
		editor.NMSObject = Vx_x_Rx.get().comple(item);
		editor.NBTTag = Vx_x_Rx.get().read(item, editor.NMSObject);
		return editor;
	}

	public static NBTEditor<Entity> open(Entity entity) {
		NBTEditor<Entity> editor = new NBTEditor<Entity>(entity);
		editor.NMSObject = Vx_x_Rx.get().comple(entity);
		editor.NBTTag = Vx_x_Rx.get().read(entity, editor.NMSObject);
		return editor;
	}

	public static NBTEditor<Block> open(Block block) {
		NBTEditor<Block> editor = new NBTEditor<Block>(block);
		editor.NMSObject = Vx_x_Rx.get().comple(block);
		editor.NBTTag = Vx_x_Rx.get().read(block, editor.NMSObject);
		return editor;
	}

	public void set(String tagname, boolean obj) {
		Vx_x_Rx.get().setNBTTagCompoundObject(NBTTag, tagname, obj);
	}

	public void set(String tagname, Number obj) {
		Vx_x_Rx.get().setNBTTagCompoundObject(NBTTag, tagname, obj);
	}

	public void set(String tagname, String obj) {
		Vx_x_Rx.get().setNBTTagCompoundObject(NBTTag, tagname, obj);
	}

	public void set(String tagname, Collection<?> obj) {
		Vx_x_Rx.get().setNBTTagCompoundObject(NBTTag, tagname, obj);
	}

	public void set(String tagname, Map<String, ?> obj) {
		Vx_x_Rx.get().setNBTTagCompoundObject(NBTTag, tagname, obj);
	}

	public void set(String tagname, Object obj) {
		Vx_x_Rx.get().setNBTTagCompoundObject(NBTTag, tagname, obj);
	}

	public Object get(String tagname) {
		return Vx_x_Rx.get().getNBTTagCompoundObject(NBTTag, tagname);
	}

	public Set<String> getKeys() {
		return Vx_x_Rx.get().getNBTTagCompoundKeys(NBTTag);
	}

	public boolean containsKey(String tagname) {
		return Vx_x_Rx.get().getNBTTagCompoundKeys(NBTTag).contains(tagname);
	}

	public Map<String, Object> toMap() {
		return new HashMap<String, Object>(){{getKeys().forEach(x -> this.put(x, get(x)));}};
	}

	public <O> T flush() {
		if(neko instanceof ItemStack) {
			((ItemStack) neko).setItemMeta(((ItemStack) Vx_x_Rx.get().buildItem(NMSObject, NBTTag)).getItemMeta());
		}else if(neko instanceof Entity) {
			Vx_x_Rx.get().buildEntity(NMSObject, NBTTag);
		}else if(neko instanceof Block) {
			Vx_x_Rx.get().buildBlock(NMSObject, NBTTag);
		}
		return neko;
	}

	public <O> T copy() {
		if(neko instanceof ItemStack) {
			neko = Vx_x_Rx.get().buildItem(NMSObject, NBTTag);
		}else if(neko instanceof Entity) {
			Vx_x_Rx.get().buildEntity(NMSObject, NBTTag);
		}else if(neko instanceof Block) {
			Vx_x_Rx.get().buildBlock(NMSObject, NBTTag);
		}
		return neko;
	}

	public static String parseNBT(Map<String, Object> mbtmap) {
		return Vx_x_Rx.get().parseNBT(mbtmap);
	}

	public static Map<String, Object> parseNBT(String nbttag) {
		return Vx_x_Rx.get().parseNBT(nbttag);
	}
}
