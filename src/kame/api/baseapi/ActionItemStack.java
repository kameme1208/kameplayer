package kame.api.baseapi;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.ItemStack;

import kame.kameplayer.Main;

public class ActionItemStack extends ItemStack {

	static {
		Bukkit.getPluginManager().registerEvents(new ActionHandler(), Main.getPlugin());
	}

	private List<ClickEvent> click = new ArrayList<>();
	private List<DragEvent> drag = new ArrayList<>();

	public ActionItemStack(Material material, int amount, short durability) {
		super(material, amount, durability);
	}

	public ActionItemStack(Material material, int amount) {
		super(material, amount);
	}

	public ActionItemStack(Material material) {
		super(material);
	}

	public ActionItemStack(ItemStack item) {
		super(item);
	}

	public void addClickEvent(ClickEvent event) {
		click.add(event);
	}

	public void addDragEvent(DragEvent event) {
		drag.add(event);
	}

	private static class ActionHandler implements Listener {

		private ActionHandler() {}
		@EventHandler
		private void onClick(InventoryClickEvent event) {
			ItemStack item = event.getCurrentItem();
			if(item instanceof ActionItemStack)((ActionItemStack)item).click.forEach(x -> x.run(event));
		}

		@EventHandler
		private void onDrag(InventoryDragEvent event) {
			ItemStack item = event.getOldCursor();
			if(item instanceof ActionItemStack)((ActionItemStack)item).drag.forEach(x -> x.run(event));
		}
	}

	public interface ClickEvent {
		public void run(InventoryClickEvent event);
	}

	public interface DragEvent {
		public void run(InventoryDragEvent event);
	}

}
