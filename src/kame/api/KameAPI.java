package kame.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import kame.api.baseapi.Actionbar;
import kame.api.baseapi.Particles;
import kame.kameplayer.event.ItemDamageEvent;

public class KameAPI {

	private KameAPI() {}

	public static void sendActionbar(Player player, String message) {
		Actionbar.sendMessage(player, message);
	}

	public static void sendParticle(Player player, String particlename, Location loc, Vector vec, int amount, double speed, double radius, boolean force, int... data) {
		Particles.sendParticle(player, particlename, loc, vec, amount, speed, radius, force, data);
	}

	public static Object getParticle(String particlename) {
		return Particles.getParticle(particlename);
	}

	public static List<String> getParticles() {
		return new ArrayList<>(Particles.getParticles());
	}

	public static Location getPlayerEye(Player player) {
		try {
			if(Bukkit.getPluginManager().isPluginEnabled("MorePlayerModels2")) {
				noppes.mpm.ModelData data = noppes.mpm.ModelData.get(player);
				return player.getLocation().add(0, ((data.body.scaleY + data.leg1.scaleY) / 2) * player.getEyeHeight(), 0);
			}
		}catch(NoSuchMethodError e) {
		}
		return player.getEyeLocation();
	}

	private static Pattern p = Pattern.compile("(durability ?: ?)([0-9]+)( ?/ ?)([0-9]+)");
	public static Result itemDamage(Player player, ItemStack item, int damage) {
		if(item == null || item.getAmount() == 0)return Result.DEFAULT;
		ItemMeta im = item.getItemMeta();
		if(!im.hasLore())return Result.DEFAULT;
		String lore = String.join("\n", im.getLore());
		Matcher m = p.matcher(lore.toLowerCase());
		if(m.find()) {
			Integer ench = im.getEnchants().get(Enchantment.DURABILITY);
			if(ench != null)damage /= 1 + Math.random() * (ench + 1);
			ItemDamageEvent event = new ItemDamageEvent(player, item, damage);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled() || event.getDamage() == 0)return Result.ALLOW;
			String line = m.group(1);
			int durability = Integer.parseInt(m.group(2));
			durability-=event.getDamage();
			lore = m.replaceFirst(line + durability + m.group(3) + m.group(4));
			im.setLore(Arrays.asList(lore.split("\n")));
			item.setItemMeta(im);
			return durability < 0 ? Result.DENY : Result.ALLOW;
		}
		return Result.DEFAULT;
	}

}
